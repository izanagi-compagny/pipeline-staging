#!/bin/bash 

while read -r line; do
  cat rereworked_variables.env | grep $line >> .env_created
done < envconfig.list
