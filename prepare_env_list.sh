#!/bin/bash

#read .env to extract variable used by application
IFS='='
while read -r line; do
  read -ra lineSplit <<< "$line"
  echo $lineSplit >> envconfig.list.tmp
done <.env
#add mandatory variable

#remove duplicate line

sort envconfig.list.tmp | uniq > envconfig.list
rm envconfig.list.tmp
